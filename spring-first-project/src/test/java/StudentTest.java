import com.vdeer.base.Student;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class StudentTest {
   @Test
    public void test(){
      ApplicationContext context= new ClassPathXmlApplicationContext("beans.xml");
       //  根据 id 获取 Bean
       //Student student= (Student) context.getBean("student");
       // 根据类型获取 Bean
      Student student=context.getBean(Student.class);
      // 根据id和类型获取 Bean
      //Student student=context.getBean("student", Student.class);
      student.study();
   }

}
