import cn.vdeer.MyBean;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MyTest {
    @Test
    void test() {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        //从 IOC 容器中获取第一次 Bean 实例
        MyBean myBean1 = context.getBean("myBean", MyBean.class);
        //从 IOC 容器中获取第二次 Bean 实例
        MyBean myBean2 = context.getBean("myBean", MyBean.class);

        //判断两个 Bean 实例是否为同一个
        if (myBean1 == myBean2) {
            System.out.println("两次返回的是同一个 Bean 实例");
        }
    }

    @Test
    void test2() {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        //从 IOC 容器中获取第一次 Bean 实例
        MyBean myBean1 = context.getBean("myBeanNew", MyBean.class);
        //从 IOC 容器中获取第二次 Bean 实例
        MyBean myBean2 = context.getBean("myBeanNew", MyBean.class);

        //判断两个 Bean 实例是否为同一个
        if (myBean1 == myBean2) {
            System.out.println("两次返回的  <是>  同一个 Bean 实例");
        } else {
            System.out.println("两次返回的  <不是>  同一个 Bean 实例");
        }

    }
}
