package cn.vdeer.impl;

import cn.vdeer.base.CalculateService;
import org.springframework.stereotype.Service;

@Service("calculateService")
public class CalculateServiceImpl implements CalculateService {
    @Override
    public int add(int a, int b) {
        int c = a + b;
        System.out.println(a + "+" + b + "=" + c);
        return c;
    }

    @Override
    public int subtract(int a, int b) {
        int c = a - b;
        System.out.println(a + "-" + b + "=" + c);
        return c;
    }

    @Override
    public int multiply(int a, int b) {
        int c = a * b;
        System.out.println(a + "x" + b + "=" + c);
        return c;
    }

    @Override
    public double divide(int a, int b) {
        int c = a / b;
        System.out.println(a + "÷" + b + "=" + c);
        return c;
    }

    @Override
    public void remainder(int a, int b) {
        int c = a % b;
        System.out.println(a + "%" + b + "=" + c);
    }
}
