package cn.vdeer;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Arrays;

/**
 * 切面类：包含五个通知方法
 */
@Component
@Aspect
public class MyAspect {
    /**
     * 前置通知：通知方法
     *
     * @param joinPoint
     */
    @Before("execution(* cn.vdeer.impl.CalculateServiceImpl.add(..))")
    public void beforeCalculate(JoinPoint joinPoint) {
        //获取方法名
        String methodName = joinPoint.getSignature().getName();
        //获取方法的参数
        String args = Arrays.toString(joinPoint.getArgs());
        System.out.println("前置通知，方法名：" + methodName + "，参数：" + args);

    }

    /**
     * 后置通知：通知方法
     *
     * @param joinPoint
     */
    @After("execution(* cn.vdeer.impl.CalculateServiceImpl.subtract(..))")
    public void afterCalculate(JoinPoint joinPoint) {
        //获取方法名
        String methodName = joinPoint.getSignature().getName();
        //获取方法的参数
        String args = Arrays.toString(joinPoint.getArgs());
        System.out.println("后置通知，方法名：" + methodName + "，参数：" + args);
    }

    /**
     * 后置返回通知：通知方法
     *
     * @param joinPoint
     * @param returnValue
     */
    @AfterReturning(value = "execution(* cn.vdeer.impl.CalculateServiceImpl.multiply(..))", returning = "returnValue")
    public void afterReturnCalculate(JoinPoint joinPoint, Object returnValue) {
        //获取方法名
        String methodName = joinPoint.getSignature().getName();
        //获取方法的参数
        String args = Arrays.toString(joinPoint.getArgs());
        System.out.println("后置返回通知，方法名：" + methodName + "，参数：" + args + "，结果：" + returnValue);

    }

    /**
     * 异常通知
     *
     * @param joinPoint
     * @param exception
     */
    @AfterThrowing(value = "execution(* cn.vdeer.impl.CalculateServiceImpl.divide(..))", throwing = "exception")
    public void afterThrowCalculate(JoinPoint joinPoint, Throwable exception) {
        //获取方法名
        String methodName = joinPoint.getSignature().getName();
        //获取方法的参数
        String args = Arrays.toString(joinPoint.getArgs());
        System.out.println("异常 通知，方法名：" + methodName + "，参数：" + args + "，异常：" + exception.getMessage());
    }

    /**
     * 环绕通知：通知方法
     *
     * @param proceedingJoinPoint
     * @throws Throwable
     */
    @Around("execution(* cn.vdeer.impl.CalculateServiceImpl.remainder(..))")
    public void aroundCalculate(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println("环绕通知---前……");
        Object proceed = proceedingJoinPoint.proceed();
        System.out.println("环绕通知---后……");
    }
}
