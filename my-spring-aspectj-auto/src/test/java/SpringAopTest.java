import cn.vdeer.base.CalculateService;
import cn.vdeer.config.AppConfig;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SpringAopTest {
    @Test
    public void test() {
        //获取容器
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        //获取 Bean 实例
        CalculateService calculateService = context.getBean("calculateService", CalculateService.class);
        //使用 Bean,调用方法
        calculateService.add(1, 2);
        System.out.println();
        calculateService.subtract(3, 1);
        System.out.println();
        calculateService.multiply(4, 2);
        System.out.println();
        calculateService.remainder(12, 10);
        System.out.println();
        try {
            calculateService.divide(9, 0);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
