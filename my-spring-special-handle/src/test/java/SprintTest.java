import com.vdeer.hd.ExampleBean;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SprintTest {
    @Test
    public void myTest(){
        ApplicationContext context= new ClassPathXmlApplicationContext("bean.xml");
        ExampleBean bean=context.getBean("exampleBean", ExampleBean.class);
        System.out.println(bean.toString());
    }


}
