package com.vdeer.hd;

public class ExampleBean {
    //Null值
    private String propertyNull;
    //空字符串
    private String propertyEmpty;
    //包含特殊符号的字面量
    private String propertyLiteral;

    public void setPropertyLiteral(String propertyLiteral) {
        this.propertyLiteral = propertyLiteral;
    }

    public void setPropertyNull(String propertyNull) {
        this.propertyNull = propertyNull;
    }

    public void setPropertyEmpty(String propertyEmpty) {
        this.propertyEmpty = propertyEmpty;
    }

    @Override
    public String toString() {
        return "ExampleBean{" +
                "propertyNull='" + propertyNull + '\'' +
                ", propertyEmpty='" + propertyEmpty + '\'' +
                ", propertyLiteral='" + propertyLiteral + '\'' +
                '}';
    }
}
