package cn.vdeer.base;

public interface CalculateService {
    /**
     * 加法
     *
     * @param a
     * @param b
     * @return
     */
    public int add(int a, int b);

    /**
     * 减法
     *
     * @param a
     * @param b
     * @return
     */
    public int subtract(int a, int b);

    /**
     * 乘法
     *
     * @param a
     * @param b
     * @return
     */
    public int multiply(int a, int b);

    /**
     * 除法
     *
     * @param a
     * @param b
     * @return
     */
    public double divide(int a, int b);

    /**
     * 取余运算
     *
     * @param a
     * @param b
     */
    public void remainder(int a, int b);
}
