import cn.vdeer.tx.entity.Order;
import cn.vdeer.tx.service.OrderService;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MyTest {
    @Test
    public void test() {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        OrderService orderService = context.getBean("orderService", OrderService.class);
        //创建订单
        Order order = new Order();
        //订单用户
        order.setUserId("1");    //用户编号
        // 产品id
        order.setProductId("001");
        //订单产品数量
        order.setCount(20);

        try {
            //执行订单，下单
            orderService.createOrder(order);
        } catch (Exception e) {
            System.err.println("!!!ERROR:" + e.getMessage());
        }

    }
}
