package cn.vdeer.tx.service.impl;

import cn.vdeer.tx.dao.OrderDao;
import cn.vdeer.tx.dao.ProductDao;
import cn.vdeer.tx.dao.UserDao;
import cn.vdeer.tx.entity.Order;
import cn.vdeer.tx.entity.Product;
import cn.vdeer.tx.service.OrderService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service("orderService")
public class OrderServiceImpl implements OrderService {
    @Resource
    private OrderDao orderDao;
    @Resource
    private UserDao userDao;
    @Resource
    private ProductDao productDao;

    /**
     * 业务层 创建订单
     *
     * @param order
     */
    @Override
    @Transactional(isolation = Isolation.DEFAULT, propagation = Propagation.REQUIRED, timeout = 10, readOnly = false)
    public void createOrder(Order order) {
        //查看商品是否存在
        Product product = productDao.selectProduct(order.getProductId());
//        System.out.println(product);
//        System.exit(1);
        //如果商品不为空，即商品存在才可继续后续操作
        if (product != null) {
            //自动生成订单 id
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
            String timeTag = sdf.format(new Date());
            String orderId = order.getUserId() + order.getProductId() + timeTag;
            //设置订单id
            order.setOrderId(orderId);
            System.out.println("开始创建订单数据，订单号为：" + orderId);

            //订单金额 = 商品单价 * 订单商品数量；
            BigDecimal multiply = (product.getPrice()).multiply(BigDecimal.valueOf(order.getCount()));
            System.out.println("订单总价：" + product.getPrice() + "x" + order.getCount() + "=" + multiply);
            //设置订单金额
            order.setOrderAmount(multiply);

            //创建订单数据
            orderDao.createOrder(order);
            System.out.println("订单数据创建完成，订单号为：" + orderId);
            System.out.println("_______________________________________________________________________");
            System.out.println("开始查询商品库存，商品 id 为：" + order.getProductId() + ", 商品名称为：" + product.getProductName());
            //查询库存数量
            int stock = productDao.selectStockByProductId(order.getProductId());
            System.out.println("商品的剩余库存为：" + stock);
            //判断库存是否充足
            if (stock >= order.getCount()) {
                System.out.println("商品库存充足，正在扣减商品库存...");
                productDao.decrease(order.getCount(), order.getProductId());
                System.out.println("商品库存扣减完成!");
            } else {
                System.out.println("警告：商品库存不足，正在执行回滚操作...！");
                throw new RuntimeException("库存不足！");
            }
            //判断用户账号金额是否充足
            System.out.println("开始查询用户的账户金额...");
            BigDecimal useMoney = userDao.getUserMoney(order.getUserId());
            System.out.println("用户:" + userDao.getUserName(order.getUserId()) + "账户余额为：" + useMoney);
            if (useMoney.intValue() >= multiply.intValue()) {
                System.out.println("账户金额充足，正在扣减账户金额...");
                userDao.decreaseMoney(order.getUserId(), multiply);
                System.out.println("账户金额扣减完成!");
            } else {
                System.out.println("警告：账户余额不足，正在执行回滚操作...！");
                throw new RuntimeException("账户余额不足");
            }
            System.out.println("开始修改订单状态，未完成》》》》》已完成");
            orderDao.updateOrderStatus(order.getOrderId(), 0);
            System.out.println("修改订单状态完成！");
        }

    }
}
