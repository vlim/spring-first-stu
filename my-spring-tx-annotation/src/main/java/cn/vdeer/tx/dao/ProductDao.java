package cn.vdeer.tx.dao;

import cn.vdeer.tx.entity.Product;

public interface ProductDao {
    /**
     * 根据商品 ID 获取商品信息
     *
     * @param productId
     * @return
     */
    Product selectProduct(String productId);

    /**
     * 查询商品的库存
     *
     * @param productId
     * @return
     */
    int selectStockByProductId(String productId);

    /**
     * 扣减商品库存
     *
     * @param
     * @return
     */
    int decrease(int count, String productId);
}
