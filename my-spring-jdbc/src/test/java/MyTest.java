import cn.vdeer.entity.User;
import cn.vdeer.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;

public class MyTest {
    @Test
    public void test() {
        //加载配置文件，获取容器
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        //获取 Bean 实例
        UserService userService = context.getBean("userService", UserService.class);
        //实例化
        User user1 = new User();
        user1.setUserName("Hogan");
        user1.setStatus("离线");
        //添加用户
        if (userService.addUser(user1) > 0) {
            System.out.println("用户:" + user1.getUserName() + " 添加成功！");
        } else {
            System.out.println("用户:" + user1.getUserName() + " 添加失败！");
        }
        User user2 = new User();
        user2.setUserName("Hogan");
        user2.setStatus("在线");
        //修改用户状态
        if (userService.updateUser(user2) > 0) {
            System.out.println("用户:" + user2.getUserName() + " 状态修改成功！");
        } else {
            System.out.println("用户:" + user2.getUserName() + " 状态修改失败！");
        }
        //批量处理
        List<Object[]> batchArgs = new ArrayList<>();
        Object[] objects1 = {"小明", "在线"};
        Object[] objects2 = {"小李", "在线"};
        Object[] objects3 = {"小刘", "离线"};
        Object[] objects4 = {"小图", "在线"};
        batchArgs.add(objects1);
        batchArgs.add(objects2);
        batchArgs.add(objects3);
        batchArgs.add(objects4);
        try {
            userService.batchAddUser(batchArgs);
            System.out.println("批量添加成功！");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        user2 = new User();
        user2.setStatus("在线");
        int i1 = userService.countUser(user2);
        System.out.println("在线用户的个数为：" + i1);
        List<User> userList = userService.getUserList(user2);
        System.out.println("在线用户列表查询成功！");
        for (User user4 : userList) {
            System.out.println("用户 ID:" + user4.getUserId() + "，用户名：" + user4.getUserName() + "，状态：" + user4.getStatus());

        }
    }
}