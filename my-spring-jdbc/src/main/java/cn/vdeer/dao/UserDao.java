package cn.vdeer.dao;

import cn.vdeer.entity.User;

import java.util.List;

public interface UserDao {
    /**
     * 新增用户
     *
     * @param user
     * @return
     */
    int addUser(User user);

    /**
     * 删除用户
     *
     * @param user
     * @return
     */
    int deleteUser(User user);

    /**
     * 修改用户信息
     *
     * @param user
     * @return
     */
    int updateUser(User user);

    /**
     * 统计用户数目
     *
     * @param user
     * @return
     */
    int count(User user);

    /**
     * 获取所有用户列表
     *
     * @param user
     * @return
     */
    List<User> getUserList(User user);

    /**
     * 获取某个用户的信息
     *
     * @param userId
     * @return
     */
    User getUser(Integer userId);

    /**
     * 批量增加用户
     *
     * @param batchArgs
     */
    void batchAddUser(List<Object[]> batchArgs);
}
