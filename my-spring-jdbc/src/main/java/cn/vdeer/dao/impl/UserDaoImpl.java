package cn.vdeer.dao.impl;

import cn.vdeer.dao.UserDao;
import cn.vdeer.entity.User;
import jakarta.annotation.Resource;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("userDao")
public class UserDaoImpl implements UserDao {
    //    获取数据源，自动装配
    @Resource
    private JdbcTemplate myjdbcTemplate;

    /**
     * 新增用户
     *
     * @param user
     * @return
     */
    @Override
    public int addUser(User user) {
        String sqltmp = "SELECT COUNT(*) FROM `user` where `user_name`=?;";
        try {
            //判断相同用户名的User是否存在
            Integer tmpCount = myjdbcTemplate.queryForObject(sqltmp, Integer.class, user.getUserName());
            System.out.println("tmpCount:" + tmpCount);
            if (tmpCount <= 0) {
                String sql = "INSERT into `user` (`user`.user_name,`user`.`status`) VALUES(?,?);";
                tmpCount = myjdbcTemplate.update(sql, user.getUserName(), user.getStatus());
                return tmpCount;
            } else {
                return 0;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return 0;
    }

    /**
     * 删除用户
     *
     * @param user
     * @return
     */
    @Override
    public int deleteUser(User user) {
        String sql = "DELETE FROM `user` where user_name=?;";
        int deleteCount = myjdbcTemplate.update(sql, user.getUserName());
        return deleteCount;
    }

    /**
     * 修改用户信息
     *
     * @param user
     * @return
     */
    @Override
    public int updateUser(User user) {
        String sql = "UPDATE `user` SET status=? WHERE user_name=?;";
        return myjdbcTemplate.update(sql, user.getStatus(), user.getUserName());
    }

    /**
     * 统计用户数目
     *
     * @param user
     * @return
     */
    @Override
    public int count(User user) {
        String sql = "SELECT COUNT(*) FROM `user` where `status`=?;";
        return myjdbcTemplate.queryForObject(sql, Integer.class, user.getStatus());
    }

    /**
     * 获取所有用户列表
     *
     * @param user
     * @return
     */
    @Override
    public List<User> getUserList(User user) {
        String sql = "SELECT * FROM `user` where `status`=?;";
        return myjdbcTemplate.query(sql, new BeanPropertyRowMapper<User>(User.class), user.getStatus());
    }

    /**
     * 获取某个用户的信息
     *
     * @param userId
     * @return
     */
    @Override
    public User getUser(Integer userId) {
        String sql = "SELECT * FROM `user` where `user_id`=?;";
        return myjdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<User>(User.class), userId);
    }

    /**
     * 批量增加用户
     *
     * @param batchArgs
     */
    @Override
    public void batchAddUser(List<Object[]> batchArgs) {
        String sql = "INSERT into `user` (`user`.user_name,`user`.`status`) VALUES(?,?);";
        myjdbcTemplate.batchUpdate(sql, batchArgs);
    }
}
