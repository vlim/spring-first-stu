package cn.vdeer.service.impl;

import cn.vdeer.dao.UserDao;
import cn.vdeer.entity.User;
import cn.vdeer.service.UserService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("userService")
public class UserServiceImpl implements UserService {
    // 定义userDao bean ，自动装配
    @Resource
    private UserDao userDao;

    /**
     * 新增用户
     *
     * @param user
     * @return
     */
    @Override
    public int addUser(User user) {
        return userDao.addUser(user);
    }

    /**
     * 删除用户
     *
     * @param user
     * @return
     */
    @Override
    public int deleteUser(User user) {
        return userDao.deleteUser(user);
    }

    /**
     * 修改用户信息
     *
     * @param user
     * @return
     */
    @Override
    public int updateUser(User user) {
        return userDao.updateUser(user);
    }

    /**
     * 统计用户数目
     *
     * @param user
     * @return
     */
    @Override
    public int countUser(User user) {
        return userDao.count(user);
    }

    /**
     * 获取所有用户列表
     *
     * @param user
     * @return
     */
    @Override
    public List<User> getUserList(User user) {
        return userDao.getUserList(user);
    }

    /**
     * 获取某个用户的信息
     *
     * @param userId
     * @return
     */
    @Override
    public User getUser(Integer userId) {
        return userDao.getUser(userId);
    }

    /**
     * 批量增加用户
     *
     * @param batchArgs
     */
    @Override
    public void batchAddUser(List<Object[]> batchArgs) {
        userDao.batchAddUser(batchArgs);
    }
}
