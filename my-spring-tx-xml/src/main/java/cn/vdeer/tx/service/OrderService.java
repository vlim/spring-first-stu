package cn.vdeer.tx.service;

import cn.vdeer.tx.entity.Order;

public interface OrderService {
    /**
     * 业务层 创建订单
     *
     * @param order
     */
    public void createOrder(Order order);
}
