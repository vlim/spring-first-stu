package cn.vdeer.tx.dao.impl;

import cn.vdeer.tx.dao.ProductDao;
import cn.vdeer.tx.entity.Product;
import jakarta.annotation.Resource;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository("productDao")
public class ProductDaoImpl implements ProductDao {
    @Resource
    private JdbcTemplate jdbcTemplate;

    /**
     * 根据商品 ID 获取商品信息
     *
     * @param productId
     * @return
     */
    @Override
    public Product selectProduct(String productId) {
        //String sql = "SELECT * FROM `product` WHERE prod_id=?";
        String sql = "SELECT  *  FROM `product` WHERE productId=?";
//        System.out.println(product.getProductName());
        return jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<Product>(Product.class), productId);
        //不能直接返回jdbcTemplate查询结果，需要先转换为实例
    }

    /**
     * 查询商品的库存
     *
     * @param productId
     * @return
     */
    @Override
    public int selectStockByProductId(String productId) {
        return this.selectProduct(productId).getStock();
    }

    /**
     * 扣减商品库存
     *
     * @param count
     * @param productId
     * @return
     */
    @Override
    public int decrease(int count, String productId) {
        String sql = " update product set  stock = stock -?  where productId=?";
        return jdbcTemplate.update(sql, count, productId);
    }
}
