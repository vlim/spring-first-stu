package cn.vdeer.tx.dao.impl;

import cn.vdeer.tx.dao.OrderDao;
import cn.vdeer.tx.entity.Order;
import jakarta.annotation.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository("orderDao")
public class OrderDaoImpl implements OrderDao {
    @Resource
    private JdbcTemplate jdbcTemplate;

    /**
     * 创建订单
     *
     * @param order
     * @return
     */
    @Override
    public int createOrder(Order order) {
        String sql = "insert into `order` (order_id,user_id, product_id, `count`, order_amount, status) values (?,?,?,?,?,?)";
        return jdbcTemplate.update(sql, order.getOrderId(), order.getUserId(), order.getProductId(), order.getCount(), order.getOrderAmount(), order.getStatus());
    }

    /**
     * 更新订单状态
     *
     * @param orderId
     * @param status
     */
    @Override
    public void updateOrderStatus(String orderId, int status) {
        String sql = "update `order`  set status = 1 where order_id = ? and status = ?;";
        jdbcTemplate.update(sql, orderId, status);
    }
}
