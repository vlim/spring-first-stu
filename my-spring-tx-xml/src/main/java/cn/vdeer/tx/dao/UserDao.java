package cn.vdeer.tx.dao;

import java.math.BigDecimal;

public interface UserDao {
    /**
     * 查看用户的账户余额
     *
     * @param userId
     * @return
     */
    BigDecimal getUserMoney(String userId);

    /**
     * 扣款
     *
     * @param userId
     * @param money
     * @return
     */
    int decreaseMoney(String userId, BigDecimal money);

    /**
     * 查看用户的名称
     *
     * @param userId
     * @return
     */
    String getUserName(String userId);
}
