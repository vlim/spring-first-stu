package cn.vdeer.tx.dao.impl;

import cn.vdeer.tx.dao.UserDao;
import cn.vdeer.tx.entity.User;
import jakarta.annotation.Resource;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository("userDao")
public class UserDaoImpl implements UserDao {
    @Resource
    private JdbcTemplate jdbcTemplate;

    /**
     * 查看用户的账户余额
     *
     * @param userId
     * @return
     */
    @Override
    public BigDecimal getUserMoney(String userId) {
        String sql = "SELECT * FROM `user` WHERE user_id=?";
        //从数据库中直接获取对象
        User user = jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<User>(User.class), userId);
        return user.getMoney();
    }

    /**
     * 扣款
     *
     * @param userId
     * @param money
     * @return
     */
    @Override
    public int decreaseMoney(String userId, BigDecimal money) {
        String sql = "UPDATE `user` SET money=money-? WHERE user_id=?";
        return jdbcTemplate.update(sql, money, userId);
    }

    /**
     * 查看用户的名称
     *
     * @param userId
     * @return
     */
    @Override
    public String getUserName(String userId) {
        String sql = "SELECT * FROM `user` WHERE user_id=?";
        //从数据库中直接获取对象
        User user = jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<User>(User.class), userId);
        return user.getUserName();
    }
}
