package cn.vdeer.tx.dao;

import cn.vdeer.tx.entity.Order;

public interface OrderDao {
    /**
     * 创建订单
     *
     * @param order
     * @return
     */
    public int createOrder(Order order);

    /**
     * 更新订单状态
     *
     * @param orderId
     * @param status
     */
    public void updateOrderStatus(String orderId, int status);
}
