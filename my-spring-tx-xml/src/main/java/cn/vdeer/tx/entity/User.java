package cn.vdeer.tx.entity;

import java.math.BigDecimal;

public class User {
    //用户 ID
    private String userId;
    //用户名
    private String userName;
    //用户账户余额
    private BigDecimal money;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }
}
