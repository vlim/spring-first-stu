import cn.vdeer.log.HelloLog4j;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MyTest {
    private static final Logger log = LoggerFactory.getLogger(MyTest.class);

    @Test
    public void test() {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        log.info("正在从容器中获取 HelloLog4j 的 Bean");
        HelloLog4j helloLog4j = context.getBean("helloLog4j", HelloLog4j.class);

        System.out.println(helloLog4j.getMessage());
        log.info("代码执行完成！");
    }
}
