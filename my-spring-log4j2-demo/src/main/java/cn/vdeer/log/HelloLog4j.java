package cn.vdeer.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HelloLog4j {
    private static final Logger log = LoggerFactory.getLogger(HelloLog4j.class);
    private String message;

    public String getMessage() {
        log.info("日志消息为：" + message);
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "HelloLog4j{" +
                "message='" + message + '\'' +
                '}';
    }
}
