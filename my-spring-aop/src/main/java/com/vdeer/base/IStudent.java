package com.vdeer.base;

public interface IStudent {
    public void study();

    public void add();

    public void delete();

    public void get();
}
