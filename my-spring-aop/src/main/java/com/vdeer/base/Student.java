package com.vdeer.base;

public class Student implements IStudent {
    public void study() {
        System.out.println("学生正在学习中...");
    }

    public void add() {
        System.out.println("Student add()...");
    }

    public void delete() {
        System.out.println("Student delete()...");
    }

    public void get() {
        System.out.println("Student get()...");
    }


}
