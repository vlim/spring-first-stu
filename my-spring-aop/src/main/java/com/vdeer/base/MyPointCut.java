package com.vdeer.base;

import org.springframework.aop.support.NameMatchMethodPointcut;

import java.lang.reflect.Method;

public class MyPointCut extends NameMatchMethodPointcut {
    @Override
    public boolean matches(Method method, Class<?> targetClass) {
        return method.getName().matches("(add|delete)");
    }
}
