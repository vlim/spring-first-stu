package com.vdeer.base;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.AfterReturningAdvice;
import org.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

public class MyAdvice implements MethodBeforeAdvice, MethodInterceptor, AfterReturningAdvice {
    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        System.out.println("环绕通知前： MyAdvice: invoke()..." + invocation.getMethod().toString());
        Object proceed = invocation.proceed();
        System.out.println("环绕通知后： MyAdvice: invoke()..." + invocation.getMethod().toString());
        return proceed;
    }

    @Override
    public void afterReturning(Object returnValue, Method method, Object[] args, Object target) throws Throwable {
        System.out.println("后置反馈通知 MyAdvice: afterReturning()..." + method.getName());
    }

    @Override
    public void before(Method method, Object[] args, Object target) throws Throwable {
        System.out.println("前置通知 MyAdvice: before()..." + method.getName());
    }
}
