import com.vdeer.base.IStudent;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class StudentTest {
    @Test
    public void test() {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        //  根据 id 获取 Bean
        //Student student= (Student) context.getBean("student");
        // 根据类型获取 Bean
        IStudent student = context.getBean("proxyBean", IStudent.class);
        // 根据id和类型获取 Bean
        //Student student=context.getBean("student", Student.class);
        student.study();
        student.add();
        student.delete();
        student.get();
    }

    @Test
    public void test1() {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans1.xml");
        //  根据 id 获取 Bean
        //Student student= (Student) context.getBean("student");
        // 根据类型获取 Bean
        IStudent student = context.getBean("student", IStudent.class);
        // 调用方法，查看效果
        student.study();
        student.add();
        student.delete();
        student.get();
    }

    @Test
    public void test2() {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans2.xml");
        //  根据 id 获取 Bean
        //Student student= (Student) context.getBean("student");
        // 根据类型获取 Bean
        IStudent student = context.getBean("student", IStudent.class);
        // 调用方法，查看效果
        student.study();
        student.add();
        student.delete();
        student.get();
    }
    
}
