package cn.vdeer.base;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class JavaCollectionNew {
    //1 数组类型属性
    private Course[] courses;
    //2 list 集合类型属性
    private List<Course> list;
    //3 map 集合类型属性
    private Map<String, Course> maps;
    //4 set 集合类型属性
    private Set<Course> sets;

    public void setCourses(Course[] courses) {
        this.courses = courses;
    }

    public void setList(List<Course> list) {
        this.list = list;
    }

    public void setMaps(Map<String, Course> maps) {
        this.maps = maps;
    }

    public void setSets(Set<Course> sets) {
        this.sets = sets;
    }

    public Course[] getCourses() {
        return courses;
    }

    public List<Course> getList() {
        return list;
    }

    public Map<String, Course> getMaps() {
        return maps;
    }

    public Set<Course> getSets() {
        return sets;
    }

    @Override
    public String toString() {
        return "JavaCollectionNew{" +
                "courses=" + Arrays.toString(courses) +
                ", list=" + list +
                ", maps=" + maps +
                ", sets=" + sets +
                '}';
    }
}
