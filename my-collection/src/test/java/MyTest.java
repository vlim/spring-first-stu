import cn.vdeer.base.Course;
import cn.vdeer.base.JavaCollection;
import cn.vdeer.base.JavaCollectionNew;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MyTest {
    @Test
    void name() {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        JavaCollection javaCollection = context.getBean("javaCollection", JavaCollection.class);
        System.out.println(javaCollection);
    }

    @Test
    void test() {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        JavaCollectionNew javaCollectionNew = context.getBean("javaCollectionNew", JavaCollectionNew.class);
        Course[] courses = javaCollectionNew.getCourses();
        List<Course> list = javaCollectionNew.getList();
        Map<String, Course> maps = javaCollectionNew.getMaps();
        Set<Course> sets = javaCollectionNew.getSets();

        System.out.println("数组：" + Arrays.toString(courses));
        System.out.println("List：" + list);
        System.out.println("Map:" + maps);
        System.out.println("set：" + sets);
    }
}
