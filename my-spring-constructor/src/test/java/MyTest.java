import com.vdeer.base.bean.Student;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MyTest {
    @Test
    public  void test(){
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        Student student=context.getBean("Student", Student.class);
        System.out.println("姓名："+student.getName());
        System.out.println("性别："+student.getGender());
        System.out.println("年龄："+student.getAge());
        System.out.println("--------------");

        student=context.getBean("Student1", Student.class);
        System.out.println("姓名："+student.getName());
        System.out.println("性别："+student.getGender());
        System.out.println("年龄："+student.getAge());
        System.out.println("--------------");

        student=context.getBean("Student2", Student.class);
        System.out.println("姓名："+student.getName());
        System.out.println("性别："+student.getGender());
        System.out.println("年龄："+student.getAge());
        System.out.println("--------------");

        student=context.getBean("Student3", Student.class);
        System.out.println("姓名："+student.getName());
        System.out.println("性别："+student.getGender());
        System.out.println("年龄："+student.getAge());
    }
}
