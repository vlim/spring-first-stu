package com.vdeer.base.bean;

public class Student {
    //姓名
    private String name;
    //年龄
    private int age;
    //姓名
    private String gender;

    /**
     * 有参构造函数
     * @param name
     * @param age
     * @param gender
     */
    public Student(String name, int age, String gender) {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
