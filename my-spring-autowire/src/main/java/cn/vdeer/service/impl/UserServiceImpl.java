package cn.vdeer.service.impl;

import cn.vdeer.dao.UserDao;
import cn.vdeer.service.UserService;

public class UserServiceImpl implements UserService {
    //定义数据存储属性
    private UserDao userDao;

    //无参数构造函数
    public UserServiceImpl() {
        System.out.println("UserServiceImpl 无参构造方法...");
    }

    //有参数构造函数
    public UserServiceImpl(UserDao userDao) {
        System.out.println("UserServiceImpl：UserDao 有参构造方法...");
        this.userDao = userDao;
    }

    public void setUserDao(UserDao userDao) {
        System.out.println("UserServiceImpl:setUserDao setter方法...");
        this.userDao = userDao;
    }

    @Override
    public void createUser() {
        System.out.println("UserServiceImpl: createUser 方法...");
        this.userDao.saveUser();
    }
}
