package cn.vdeer.controller;

import cn.vdeer.service.UserService;

public class UserController {
    private UserService userService111;

    public UserController() {
        System.out.println("UserController: 无参数 构造函数 方法...");
    }

    public UserController(UserService userService) {
        System.out.println("UserController: 有参数 构造函数 方法...");
        this.userService111 = userService;
    }

    public void setUserService(UserService userService) {
        System.out.println("UserController:setUserService  setter方法...");
        this.userService111 = userService;
    }

    public void createUser() {
        System.out.println("UserController:createUser() 方法...");
        this.userService111.createUser();
    }
}
