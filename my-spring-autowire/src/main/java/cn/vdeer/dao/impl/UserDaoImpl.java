package cn.vdeer.dao.impl;

import cn.vdeer.dao.UserDao;

public class UserDaoImpl implements UserDao {
    public UserDaoImpl() {
        System.out.println("UserDaoImpl:无参数构造函数");
    }

    @Override
    public void saveUser() {
        System.out.println("UserDaoImpl:public void saveUser()");
    }
}
