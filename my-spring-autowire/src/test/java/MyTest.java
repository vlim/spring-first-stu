import cn.vdeer.controller.UserController;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MyTest {
    @Test
    public void test() {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans-no.xml");
        UserController userController = context.getBean("userController", UserController.class);
        userController.createUser();
    }

    @Test
    public void testByName() {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans-byName.xml");
        UserController userController = context.getBean("userController", UserController.class);
        userController.createUser();
    }

    @Test
    public void testByType() {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans-byType.xml");
        UserController userController = context.getBean("userController1", UserController.class);
        userController.createUser();
    }

    @Test
    public void testByConstructor() {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans-constructor.xml");
        UserController userController = context.getBean("userController2", UserController.class);
        userController.createUser();
    }

    @Test
    public void testByDefault() {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans-default.xml");
        UserController userController = context.getBean("userController3", UserController.class);
        userController.createUser();
    }

}
