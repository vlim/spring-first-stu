import cn.vdeer.base.Student;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class MyTest {
    @Test
    public void test() {
        ApplicationContext context=new ClassPathXmlApplicationContext("student.xml");
        Student student=context.getBean("student", Student.class);
        System.out.println(student);
    }
}
