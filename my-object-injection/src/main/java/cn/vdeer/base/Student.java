package cn.vdeer.base;

import java.beans.ConstructorProperties;

public class Student {
    private String name;
    private int age;
    private School school;

    public Student() {

    }
    @ConstructorProperties({"name","age","school"})
    public Student(String name, int age, School school) {
        this.name = name;
        this.age = age;
        this.school = school;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setSchool(School school) {
        this.school = school;
    }
    /**
     * @return
     * 为级联属性赋值专门增加的 getter 方法
     */
    public School getSchool() {
        return school;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", school=" + school +
                '}';
    }
}
