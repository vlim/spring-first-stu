import cn.vdeer.base.User;
import org.junit.jupiter.api.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MyTest {
    @Test
    public void test() {
        //IOC 容器加载配置文件
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        //获取 Bean 实例
        User user = context.getBean("user", User.class);
        //使用 Bean
        System.out.println("9. 用户名：" + user.getUserName());
        //IOC 容器 关闭
        context.close();
    }

}
