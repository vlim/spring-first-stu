package cn.vdeer.base;

public class User {
    private String userName;

    public User() {
        System.out.println("2.正在调用构造方法，实例化 Bean...");
    }

    public String getUserName() {
        System.out.println(" 正在调用 getter 方法获取 Bean  属性...");
        return userName;
    }

    public void setUserName(String userName) {
        System.out.println("5.正在调用 setter 方法为 Bean 设置属性...");
        this.userName = userName;
    }

    public void initMethod() {
        System.out.println("7.正在进行 Bean 初始化...");
    }

    public void destoryMethod() {
        System.out.println("10.正在 Bean 销毁...");
    }

}
